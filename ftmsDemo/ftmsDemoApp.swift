//
//  ftmsDemoApp.swift
//  ftmsDemo
//
//  Created by Steve Liao on 2021/7/22.
//

import SwiftUI

@main
struct ftmsDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
