//
//  Extensions.swift
//  FitDisplay
//
//  Created by jht on 2020/5/27.
//  Copyright © 2020 Johnsonfitness. All rights reserved.
//

import Foundation
import SwiftUI

extension View {
        
    var isIPad: Bool {
        UIDevice.current.userInterfaceIdiom == .pad
    }
    
    func setNavbar() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithDefaultBackground()
        appearance.backgroundColor = UIColor(named: "Whisper")
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
    }
    
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    func withKeyboard(_ space: Binding<CGFloat>) {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main) { key in
            let value = key.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            space.wrappedValue = value.height
        }

        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: .main) { key in
            space.wrappedValue = 0
        }
    }
}

extension UInt8 {
    func toBits() -> [Bool] {
        var byte = self
        var bits = [Bool](repeating: false, count: 8)
        for i in 0 ..< 8 {
            let bit = byte & 0x01
            if bit != 0 {
                bits[i] = true
            }
            byte >>= 1
        }
        return bits
    }
    
    func readByBits(_ length: UInt8) -> UInt8 {
        var byte = self
        var int:UInt8 = 0
        for i in 1 ..< length + 1 {
            int += (byte & 0x01) * (i + 1)
            byte >>= 1
        }
        return int
    }
}

extension Int {
    func to2Bytes() -> [UInt8] {
        let uint = UInt16.init(Double.init(self))
        return [
            UInt8(truncatingIfNeeded: uint),
            UInt8(truncatingIfNeeded: uint >> 8)
        ]
    }

    func to3Bytes() -> [UInt8] {
        let uint = UInt32.init(Double.init(self))
        return [
            UInt8(truncatingIfNeeded: uint),
            UInt8(truncatingIfNeeded: uint >> 8),
            UInt8(truncatingIfNeeded: uint >> 16)
        ]

    }
}

extension Data {
    func readUInt16(_ offset: Int = 0) -> UInt16 {
        if offset + 1 >= count {
            return 0
        }
        let subdata = self.subdata(in: offset ..< offset + 2)
        return subdata.withUnsafeBytes { bytes in
            bytes.load(as: UInt16.self)
        }
    }
    
    func readInt16(_ offset: Int = 0) -> Int16 {
        let uint = readUInt16(offset)
        return Int16(bitPattern: uint)
    }

    func readUInt24(_ offset: Int = 0) -> UInt32 {
        if offset + 2 >= count {
            return 0
        }
        var subdata = self.subdata(in: offset ..< offset + 3)
        subdata.append(contentsOf: [0x00])
        return subdata.withUnsafeBytes { bytes in
            bytes.load(as: UInt32.self)
        }
    }
}

extension Double {
    func rounded(_ places: Int) -> Double {
        let multiplier = pow(10, Double(places))
        return (self * multiplier).rounded() / multiplier
    }
    
    func toString() -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 16 //maximum digits in Double after dot (maximum precision)
        return String(formatter.string(from: number) ?? "")
    }
    
    func toString(_ places: Int) -> String {
        let n = rounded(places)
        if places == 0 {
            return String(Int(n))
        }
        else {
            return String(n)
        }
    }
}

extension Float {
    func rounded(_ places: Int) -> Float {
        let multiplier = pow(10, Float(Double(places)))
        return (self * multiplier).rounded() / multiplier
    }
    
    func toString(_ places: Int) -> String {
        return String(self.rounded(places))
    }
}

extension String {
    func match(_ pattern: String) -> Bool {
        let regEx: NSRegularExpression
        do {
            regEx = try NSRegularExpression(pattern: pattern)
        } catch {
            return false
        }
        let matches = regEx.matches(in: self, range: NSRange(location: 0, length: self.count))
        
        return matches.count > 0
    }
    
    func unitFor(_ number: Double) -> String {
        if self == "km" || self == "h" || self == "kg" || self == "cm" || self == "kcal" {
            return self
        }
        if number > 1 {
            return self + "s"
        }
        else {
            return self
        }
    }
}

extension Calendar {
    static let gregorian = Calendar(identifier: .gregorian)
}

extension Date {
    
    static public var Months: [String] = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "Septemper",
        "October",
        "November",
        "December"
    ]
    
    var year: Int {
        let calendar = Calendar.gregorian
        let components = calendar.dateComponents([.year], from: self)
        return components.year!
    }
    
    var month: Int {
        let calendar = Calendar.gregorian
        let components = calendar.dateComponents([.month], from: self)
        return components.month!
    }
    
    var day: Int {
        let calendar = Calendar.gregorian
        let components = calendar.dateComponents([.day], from: self)
        return components.day!
    }
    
    var startOfYear: Date {
        let calendar = Calendar.gregorian
        var components = calendar.dateComponents([.year], from: self)
        components.month = 1
        components.day = 1
        return  calendar.date(from: components)!
    }
    
    var endOfYear: Date {
        let calendar = Calendar.gregorian
        var components = calendar.dateComponents([.year], from: self)
        components.month = 12
        components.day = 31
        return  calendar.date(from: components)!
    }
    
    var startOfMonth: Date {
        let calendar = Calendar.gregorian
        let components = calendar.dateComponents([.year, .month], from: self)
        return  calendar.date(from: components)!
    }

    var endOfMonth: Date {
        let calendar = Calendar.gregorian
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return calendar.date(byAdding: components, to: startOfMonth)!
    }
    
    var startOfWeek: Date {
        let calendar = Calendar.gregorian
        var components = calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)
        components.weekday = (components.weekday ?? 1) + 1
        return calendar.date(from: components)!
    }
    
    var endOfWeek: Date {
        let calendar = Calendar.gregorian
        var components = DateComponents()
        components.weekday = 7
        components.second = -1
        return calendar.date(byAdding: components, to: startOfWeek)!
    }
    
    func toString(_ format: String = "yyyy/MM/dd hh:mm") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
