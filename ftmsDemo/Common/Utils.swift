//
//  Utils.swift
//  bletouch
//
//  Created by jht on 2020/5/12.
//  Copyright © 2020 Johnsonfitness. All rights reserved.
//

import Foundation

func km2mile(_ km: Float) -> Float {
    km / 1.609344
}

func mile2km(_ mile: Float) -> Float {
    mile * 1.609344
}

func matchUnitFromKM(_ km: Float) -> Float {
    km
}

func matchSpeedForFTMS(_ speed: Float) -> Float {
   speed
}




