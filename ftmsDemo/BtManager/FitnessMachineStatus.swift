//
//  FitnessMachineStatus.swift
//  bletouch
//
//  Created by jht on 2020/5/13.
//  Copyright © 2020 Johnsonfitness. All rights reserved.
//

import Foundation
import CoreBluetooth

struct FitnessMachineStatus: BluetoothCharacteristic {
    static let id = CBUUID(string: "0x2ADA")
    
    let name = "Fitness Machine Status"
    let status: Status
    
    var value: Double? = nil
    var isStart: Bool {
        status != .FitnessMachineStoppedorPausedbytheUser && status != .FitnessMachineStoppedbySafetyKey
    }
    var isPaused: Bool {
        status == .FitnessMachineStoppedorPausedbytheUser && value == 2
    }
    var isStopped: Bool {
        status == .FitnessMachineStoppedorPausedbytheUser && value == 1
    }
  
    init(_ data: Data) {
        status = Status(rawValue: data[0]) ?? .ReservedforFutureUse
        switch status {
        case .FitnessMachineStoppedorPausedbytheUser:
            value = Double(data[1])
        case .TargetSpeedChanged:
            value = Double(data.readUInt16(1)) * 0.01
        case .TargetInclineChanged:
            value = Double(data.readInt16(1)) * 0.1
        case .TargetResistanceLevelChanged:
            value = Double(data[1]) * 0.1
        case .TargetPowerChanged:
            value = Double(data.readInt16(1))
        case .TargetHeartRateChanged:
            value = Double(data[1])
        case .TargetedExpendedEnergyChanged:
            value = Double(data.readUInt16(1))
        default:
            break
        }
    }
    
    enum Status: UInt8 {
        case ReservedforFutureUse = 0x00
        case Reset = 0x01
        case FitnessMachineStoppedorPausedbytheUser = 0x02
        case FitnessMachineStoppedbySafetyKey = 0x03
        case FitnessMachineStartedorResumedbytheUser = 0x04
        case TargetSpeedChanged = 0x05
        case TargetInclineChanged = 0x06
        case TargetResistanceLevelChanged = 0x07
        case TargetPowerChanged = 0x08
        case TargetHeartRateChanged = 0x09
        case TargetedExpendedEnergyChanged = 0x0A
        case TargetedNumberofStepsChanged = 0x0B
        case TargetedNumberofStridesChanged = 0x0C
        case TargetedDistanceChanged = 0x0D
        case TargetedTrainingTimeChanged = 0x0E
        case TargetedTimeinTwoHeartRateZonesChanged = 0x0F
        case TargetedTimeinThreeHeartRateZonesChanged = 0x10
        case TargetedTimeinFiveHeartRateZonesChanged = 0x11
        case IndoorBikeSimulationParametersChanged = 0x12
        case WheelCircumferenceChanged = 0x13
        case SpinDownStatus = 0x14
        case TargetedCadenceChanged = 0x15
        case ControlPermissionLost = 0xFF
    }
}
