//
//  ControlPoint.swift
//  bletouch
//
//  Created by jht on 2020/5/14.
//  Copyright © 2020 Johnsonfitness. All rights reserved.
//

import Foundation
import CoreBluetooth

struct FitnessMachineControlPoint: BluetoothCharacteristic {
    static let id = CBUUID(string: "0x2AD9")
    let name = "Fitness Machine Control Point"
}

class ResponseCode {
    
    var opCode: UInt8
    var requestOpCode: UInt8
    var resultCode: ResultCode
    
    init(_ data: Data) {
        opCode = data[0]
        requestOpCode = data[1]
        resultCode = ResultCode(rawValue: data[2]) ?? .OperationFailed
    }
    
    enum ResultCode: UInt8 {
        case Success = 0x01
        case OpCodeNotSupported = 0x02
        case InvalidParameter = 0x03
        case OperationFailed = 0x04
        case ControlNotPermitted = 0x05
    }
}
