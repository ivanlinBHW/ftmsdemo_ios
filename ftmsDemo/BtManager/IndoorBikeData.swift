//
//  IndoorBike.swift
//  bletouch
//
//  Created by jht on 2020/5/25.
//  Copyright © 2020 Johnsonfitness. All rights reserved.
//

import Foundation
import CoreBluetooth

struct IndoorBikeData: BluetoothCharacteristic {
    static var id = CBUUID(string: "0x2AD2")
    let name = "Indoor Bike Data"
    
    let hasMoreData: Bool
    let isAverageSpeedPresent: Bool
    let isInstantaneousCadencePresent: Bool
    let isAverageCadencePresent: Bool
    let isTotalDistancePresent: Bool
    let isResistanceLevelPresent: Bool
    let isInstantaneousPowerPresent: Bool
    let isAveragePowerPresent: Bool
    let isExpendedEnergyPresent: Bool
    var isHeartRatePresent: Bool
    let isMetabolicEquivalentPresent: Bool
    let isElapsedTimePresent: Bool
    let isRemainingTimePresent: Bool
    
    var _instantaneousSpeed: Float?
    var instantaneousSpeed: Float? {
        get {
            matchUnitFromKM(_instantaneousSpeed ?? 0)
        }
        set {
            _instantaneousSpeed = newValue
        }
    }
    
    var averageSpeed: Float?
    var instantaneousCadence: Float?
    var averageCadence: Float?
    var totalDistance: Float?
    var resistanceLevel: Int16?
    var instantaneousPower: Int16?
    var averagePower: Int16?
    var totalEnergy: UInt16?
    var energyPerHour: UInt16?
    var energyPerMinute: UInt8?
    var heartRate: UInt8?
    var metabolicEquivalent: Float?
    var elapsedTime: UInt16 = 0
    var remainingTime: UInt16?
    
    public func merge(_ data: Data) -> IndoorBikeData {
        var newData = IndoorBikeData(data)
        if newData.hasMoreData {
            newData._instantaneousSpeed = _instantaneousSpeed
        }
        if !newData.isAverageSpeedPresent {
            newData.averageSpeed = averageSpeed
        }
        if !newData.isInstantaneousCadencePresent {
            newData.instantaneousCadence = instantaneousCadence
        }
        if !newData.isAverageCadencePresent {
            newData.averageCadence = averageCadence
        }
        if !newData.isTotalDistancePresent {
            newData.totalDistance = totalDistance
        }
        if !newData.isResistanceLevelPresent {
            newData.resistanceLevel = resistanceLevel
        }
        if !newData.isInstantaneousPowerPresent {
            newData.instantaneousPower = instantaneousPower
        }
        if !newData.isAveragePowerPresent {
            newData.averagePower = averagePower
        }
        if !newData.isExpendedEnergyPresent {
            newData.totalEnergy = totalEnergy
            newData.energyPerHour = energyPerHour
            newData.energyPerMinute = energyPerMinute
        }
        if !newData.isHeartRatePresent {
            newData.isHeartRatePresent = isHeartRatePresent
            newData.heartRate = heartRate
        }
        if !newData.isMetabolicEquivalentPresent {
            newData.metabolicEquivalent = metabolicEquivalent
        }
        if !newData.isElapsedTimePresent {
            newData.elapsedTime = elapsedTime + 1
        }
        if !newData.isRemainingTimePresent {
            newData.remainingTime = remainingTime
        }
        
        return newData
    }
    
    init(_ data: Data) {
        var offset = 0
        var bit: UInt8
        var byte: UInt8
        
        byte = data[offset]
        bit = byte & 0x01
        hasMoreData = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isAverageSpeedPresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isInstantaneousCadencePresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isAverageCadencePresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTotalDistancePresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isResistanceLevelPresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isInstantaneousPowerPresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isAveragePowerPresent = bit == 1
        
        offset += 1
        byte = data[offset]
        bit = byte & 0x01
        isExpendedEnergyPresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isHeartRatePresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isMetabolicEquivalentPresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isElapsedTimePresent = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isRemainingTimePresent = bit == 1
        
        offset += 1
        if !hasMoreData {
            instantaneousSpeed = Float(data.readUInt16(offset)) * 0.01
            offset += MemoryLayout<UInt16>.size
        }
        if isAverageSpeedPresent {
            averageSpeed = Float(data.readInt16(offset)) * 0.01
            offset += MemoryLayout<UInt16>.size
        }
        if isInstantaneousCadencePresent {
            instantaneousCadence = Float(data.readUInt16(offset)) * 0.5
            offset += MemoryLayout<UInt16>.size
        }
        if isAverageCadencePresent {
            averageCadence = Float(data.readUInt16(offset)) * 0.5
            offset += MemoryLayout<UInt16>.size
        }
        if isTotalDistancePresent {
            totalDistance = Float(data.readUInt24(offset)) * 0.001
            offset += (MemoryLayout<UInt16>.size + MemoryLayout<UInt8>.size)
        }
        if isResistanceLevelPresent {
            resistanceLevel = data.readInt16(offset)
            offset += MemoryLayout<UInt16>.size
        }
        if isInstantaneousPowerPresent {
            instantaneousPower = data.readInt16(offset)
            offset += MemoryLayout<UInt16>.size
        }
        if isAveragePowerPresent {
            averagePower = data.readInt16(offset)
            offset += MemoryLayout<UInt16>.size
        }
        if isExpendedEnergyPresent {
            totalEnergy = data.readUInt16(offset)
            offset += MemoryLayout<UInt16>.size
            energyPerHour = data.readUInt16(offset)
            offset += MemoryLayout<UInt16>.size
            energyPerMinute = data[offset]
            offset += MemoryLayout<UInt8>.size
        }
        if isHeartRatePresent {
            heartRate = data[offset]
            offset += MemoryLayout<UInt8>.size
        }
        if isMetabolicEquivalentPresent {
            metabolicEquivalent = Float(data[offset]) * 0.1
            offset += MemoryLayout<UInt8>.size
        }
        if isElapsedTimePresent {
            elapsedTime = data.readUInt16(offset)
            offset += MemoryLayout<UInt16>.size
        }
        if isRemainingTimePresent {
            remainingTime = data.readUInt16(offset)
        }
    }
}
