//
//  DataReader.swift
//  bletouch
//
//  Created by jht on 2020/5/5.
//  Copyright © 2020 Johnsonfitness. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol BluetoothCharacteristic {
    static var id: CBUUID { get }
    var name: String { get }
}

protocol BCRange: BluetoothCharacteristic {
    var unit: String { get }
    var min: Float { get }
    var max: Float { get }
    var increment: Float { get }
}

struct SupportedSpeedRange: BCRange {
    static let id = CBUUID(string: "0x2AD4")
    let name = "Supported speed range"
    static let resolution: Float = 0.01
    let unit = "KM / H"
    var min: Float {
        SupportedSpeedRange.speedWithUnit(orgMin)
    }
    var max: Float {
        SupportedSpeedRange.speedWithUnit(orgMax)
    }
    let orgMin: UInt16
    let orgMax: UInt16
    let increment: Float
    
    init(_ data: Data) {
        var offset = 0
        orgMin = data.readUInt16()
        
        offset += MemoryLayout<UInt16>.size
        orgMax = data.readUInt16(offset)
        
        offset += MemoryLayout<UInt16>.size
        increment = Float(data.readUInt16(offset)) * SupportedSpeedRange.resolution
    }
    
    static func speedWithUnit(_ speed: UInt16) -> Float {
        matchUnitFromKM(Float(speed) * resolution)
    }
}

struct SupportedInclinationRange: BCRange {
    static let id = CBUUID(string: "0x2AD5")
    let name = "Supported inclination range"
    let resolution: Float = 0.1
    let unit = "%"
    let min: Float
    let max: Float
    let increment: Float
    
    init(_ data: Data) {
        var offset = 0
        min = Float(data.readInt16()) * resolution
        offset += MemoryLayout<UInt16>.size
        max = Float(data.readInt16(offset)) * resolution
        offset += MemoryLayout<UInt16>.size
        increment = Float(data.readUInt16(offset)) * resolution
    }
}

struct SupportedResistanceLevelRange: BCRange {
    static let id = CBUUID(string: "0x2AD6")
    let name = "Supported resistance level range"
    let resolution: Float = 0.1
    let unit = ""
    let min: Float
    let max: Float
    let increment: Float
    
    init(_ data: Data) {
        var offset = 0
        min = Float(data.readInt16()) * resolution
        offset += MemoryLayout<UInt16>.size
        max = Float(data.readInt16(offset)) * resolution
        offset += MemoryLayout<UInt16>.size
        increment = Float(data.readUInt16(offset)) * resolution
    }
}

struct SupportedHeartRateRange: BCRange {
    static let id = CBUUID(string: "0x2AD7")
    let name = "Supported heart rate range"
    let unit = "BPM"
    let min: Float
    let max: Float
    let increment: Float
    
    init(_ data: Data) {
        min = Float(data[0])
        max = Float(data[1])
        increment = Float(data[2])
    }
}

struct SupportedPowerRange: BCRange {
    static let id = CBUUID(string: "0x2AD8")
    let name = "Supported power range"
    let unit = "W"
    let min: Float
    let max: Float
    let increment: Float
    
    init(_ data: Data) {
        var offset = 0
        min = Float(data.readInt16())
        offset += MemoryLayout<UInt16>.size
        max = Float(data.readInt16(offset))
        offset += MemoryLayout<UInt16>.size
        increment = Float(data.readUInt16(offset))
    }
}

struct ControlPoint: BluetoothCharacteristic {
    static let id = CBUUID(string: "0x2AD9")
    let name = "Fitness Machine Control Point"
    
    static let requestControl: UInt8 = 0x00
    static let reset: UInt8 = 0x01
    static let setTargetSpeed: UInt8 = 0x02
    static let setTargetInclination: UInt8 = 0x03
    static let setTargetResistanceLevel: UInt8 = 0x04
    static let setTargetPower: UInt8 = 0x05
    static let setTargetHeartRate: UInt8 = 0x06
    static let startOrResume: UInt8 = 0x07
    static let stopOrPause: UInt8 = 0x08
    static let setTargetedExpendedEnergy: UInt8 = 0x09
    static let setTargetedNumberOfSteps: UInt8 = 0x0A
    static let setTargetedNumberOfStrides: UInt8 = 0x0B
    static let setTargetedDistance: UInt8 = 0x0C
    static let setTargetedTrainingTime: UInt8 = 0x0D
    static let setTargetedTimeInTwoHeartRateZones: UInt8 = 0x0E
    static let setTargetedTimeInThreeHeartRateZones: UInt8 = 0x0F
    static let setTargetedTimeInFiveHeartRateZones: UInt8 = 0x10
    static let setIndoorBikeSimulation: UInt8 = 0x11
    static let setWheelCircumference: UInt8 = 0x12
    static let spinDownControl: UInt8 = 0x13
    static let setTargetedCadence: UInt8 = 0x14
    static let setSecurityKey: UInt8 = 0x70
    static let responseCode: UInt8 = 0x80
}
