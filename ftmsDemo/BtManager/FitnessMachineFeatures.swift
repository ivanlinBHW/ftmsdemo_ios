//
//  FitnessMachineFeatures.swift
//  bletouch
//
//  Created by jht on 2020/5/8.
//  Copyright © 2020 Johnsonfitness. All rights reserved.
//

import Foundation
import CoreBluetooth

struct FitnessMachineFeatures: BluetoothCharacteristic {
    static var id = CBUUID(string: "0x2ACC")
    let name = "Fitness Machine Feature"
    
    let isAverageSpeedSupported: Bool
    let isCadenceSupported: Bool
    let isTotalDistanceSupported: Bool
    let isInclinationSupported: Bool

    let isElevationGainSupported: Bool
    let isPaceSupported: Bool
    let isStepCountSupported: Bool
    let isResistanceLevelSupported: Bool
    
    let isStrideCountSupported: Bool
    let isExpendedEnergySupported: Bool
    let isHeartRateMeasurementSupported: Bool
    let isMetabolicEquivalentSupported: Bool
    
    let isElapsedTimeSupported: Bool
    let isRemainingTimeSupported: Bool
    let isPowerMeasurementSupported: Bool
    let isForceOnBeltAndPowerOutputSupported: Bool
    let isUserDataRetentionSupported: Bool
    
    let isSpeedTargetSettingSupported: Bool
    let isInclinationTargetSettingSupported: Bool
    let isResistanceTargetSettingSupported: Bool
    let isPowerTargetSettingSupported: Bool
    
    let isHeartRateTargetSettingSupported: Bool
    let isTargetedExpendedEnergyConfigurationSupported: Bool
    let isTargetedStepNumberConfigurationSupported: Bool
    let isTargetedStrideNumberConfigurationSupported: Bool
    
    let isTargetedDistanceConfigurationSupported: Bool
    let isTargetedTrainingTimeConfigurationSupported: Bool
    let isTargetedTimeInTwoHeartRateZonesConfigurationSupported: Bool
    let isTargetedTimeInThreeHeartRateZonesConfigurationSupported: Bool
    
    let isTargetedTimeInFiveHeartRateZonesConfigurationSupported: Bool
    let isIndoorBikeSimulationParametersSupported: Bool
    let isWheelCircumferenceConfigurationSupported: Bool
    let isSpinDownControlSupported: Bool
    let isTargetedCadenceConfigurationSupported: Bool
    
    init(_ data: Data) {
        var bit: UInt8
        var byte: UInt8
        
        byte = data[0]
        bit = byte & 0x01
        isAverageSpeedSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isCadenceSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTotalDistanceSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isInclinationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isElevationGainSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isPaceSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isStepCountSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isResistanceLevelSupported = bit == 1

        byte = data[1]
        bit = byte & 0x01
        isStrideCountSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isExpendedEnergySupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isHeartRateMeasurementSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isMetabolicEquivalentSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isElapsedTimeSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isRemainingTimeSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isPowerMeasurementSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isForceOnBeltAndPowerOutputSupported = bit == 1
        
        byte = data[2]
        bit = byte & 0x01
        isUserDataRetentionSupported = bit == 1
        
        byte = data[4]
        bit = byte & 0x01
        isSpeedTargetSettingSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isInclinationTargetSettingSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isResistanceTargetSettingSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isPowerTargetSettingSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isHeartRateTargetSettingSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTargetedExpendedEnergyConfigurationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTargetedStepNumberConfigurationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTargetedStrideNumberConfigurationSupported = bit == 1

        byte = data[5]
        bit = byte & 0x01
        isTargetedDistanceConfigurationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTargetedTrainingTimeConfigurationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTargetedTimeInTwoHeartRateZonesConfigurationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTargetedTimeInThreeHeartRateZonesConfigurationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isTargetedTimeInFiveHeartRateZonesConfigurationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isIndoorBikeSimulationParametersSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isWheelCircumferenceConfigurationSupported = bit == 1
        byte >>= 1
        bit = byte & 0x01
        isSpinDownControlSupported = bit == 1
        
        byte = data[6]
        bit = byte & 0x01
        isTargetedCadenceConfigurationSupported = bit == 1
    }
}
