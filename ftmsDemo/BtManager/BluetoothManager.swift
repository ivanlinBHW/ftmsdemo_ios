//
//  BLE.swift
//  bletouch
//
//  Created by jht on 2020/4/28.
//  Copyright © 2020 Johnsonfitness. All rights reserved.
//

import Foundation
import CoreBluetooth
import SwiftUI

class BluetoothManager:NSObject, CBCentralManagerDelegate, CBPeripheralDelegate, ObservableObject {
    
    static let FTMS = CBUUID(string: "0x1826")
    static let SCAN_SEC = 3.0
    static let CONNECT_SEC = 5.0
    static let sharedInstance = BluetoothManager()
    
    @Published var foundPeripheralList = [UUID]()
    @Published var indoorBikeData: IndoorBikeData?
    @Published var fitnessMachineStatus: FitnessMachineStatus?
    @Published var fitnessMachine: CBPeripheral?
    @Published var discoveredPeripherals = Set<UUID>()
    @Published var connectingPeripheralId: UUID?
    
    
    public var scanType = [FTMS]
    public var cbUUIDs = [UUID:[CBUUID]]()
    public var fitnessMachineFeatures: FitnessMachineFeatures?
    public var supportedSpeedRange: SupportedSpeedRange?
    public var supportedInclinedRange: SupportedInclinationRange?
    public var supportedResistanceLevelRange: SupportedResistanceLevelRange?
   
    
    private var peripherals = [UUID:CBPeripheral]()
    private var controlPoint: CBCharacteristic?
    private var indoorBikeDataCharacteristic: CBCharacteristic?
    private var fitnessMachineStatusCharacteristic: CBCharacteristic?
    private var centralManager: CBCentralManager!
    private var stopScanTask: DispatchWorkItem?
    private var _stopConnectTask: DispatchWorkItem? = nil
    private var stopConnectTask: DispatchWorkItem {
        get {
            if _stopConnectTask?.isCancelled ?? true {
                _stopConnectTask = DispatchWorkItem {
                    self.stopConnect()
                }
                return _stopConnectTask!
            }
            else {
                return _stopConnectTask!
            }
        }
    }
        
    override private init() {
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.global())
    }
    
    func startScan() {
        DispatchQueue.main.async { [self] in
            discoveredPeripherals = Set<UUID>()
            foundPeripheralList = []
        }
        centralManager.scanForPeripherals(withServices: scanType)
        stopScanTask = DispatchWorkItem {
            self.stopScan()
        }
        DispatchQueue.global().asyncAfter(deadline: .now() + BluetoothManager.SCAN_SEC, execute: stopScanTask!)
    }
    
    func stopScan() {
        centralManager.stopScan()
        if let task = stopScanTask {
            if !task.isCancelled {
                task.cancel()
            }
            self.stopScanTask = nil
        }
    }
        

    func connect(_ id: UUID) {
        guard let peripheral = peripheral(id) else { return }
        print("connect to \(peripheralName(id)) UUID: \(id)")
        stopConnect()
        centralManager.connect(peripheral)
        DispatchQueue.main.async {
            self.connectingPeripheralId = id
        }
        DispatchQueue.global().asyncAfter(deadline: .now() + BluetoothManager.CONNECT_SEC, execute: stopConnectTask)
        peripherals.updateValue(peripheral, forKey: id)
    }
    
    func stopConnect() {
        if !stopConnectTask.isCancelled {
            stopConnectTask.cancel()
        }
        guard let id = connectingPeripheralId, let peripheral = peripheral(id) else { return }
        centralManager.cancelPeripheralConnection(peripheral)
        DispatchQueue.main.async {
            self.connectingPeripheralId = nil
        }
    }
    
    func disConnectFitnessMachine() {
        guard let peripheral = fitnessMachine else { return }
        centralManager.cancelPeripheralConnection(peripheral)
        fitnessMachine = nil
    }
    
    func disConnect(id: UUID) {
        guard let peripheral = peripherals[id] else { return }
        centralManager.cancelPeripheralConnection(peripheral)
        peripherals.updateValue(peripheral, forKey: id)
    }
        
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            print("BT ON")
        default:
            print("BT OFF")
            DispatchQueue.main.async {
                self.fitnessMachine = nil
            }
        }
    }
    
    //MARK: CentralManager
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        guard peripheral.name != nil else { return }
        
        let id = peripheral.identifier
        peripherals.updateValue(peripheral, forKey: id)
                
        if let cbUUID = advertisementData["kCBAdvDataServiceUUIDs"] as? [CBUUID] {
            cbUUIDs.updateValue(cbUUID, forKey: id)
        }
        
        DispatchQueue.main.async { [self] in
            if discoveredPeripherals.insert(id).inserted {
                switch scanType[0] {
                case BluetoothManager.FTMS:
                    self.foundPeripheralList.append(id)
                default:
                    break
                }
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("connected: \(peripheralName(peripheral.identifier))")
        peripherals.updateValue(peripheral, forKey: peripheral.identifier)
        DispatchQueue.main.async {
            self.connectingPeripheralId = nil
            let type = self.scanType[0]
            if type == BluetoothManager.FTMS {
                if self.fitnessMachine != nil {
                    self.disConnect(id: self.fitnessMachine!.identifier)
                }
                self.fitnessMachine = peripheral
            }
        }
        peripheral.delegate = self
        peripheral.discoverServices(scanType)
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("disConnected: \(peripheralName(peripheral.identifier))")
        peripheral.delegate = nil
        
        peripherals.updateValue(peripheral, forKey: peripheral.identifier)
        DispatchQueue.main.async {
            if peripheral == self.fitnessMachine {
                self.fitnessMachine = nil
            }
        }
    }
    
    //MARK: Peripheral Manager
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard error == nil else {
            print(error ?? "no errors")
            return
        }
        guard let services = peripheral.services else { return }
        for service in services {
            switch service.uuid {
            case BluetoothManager.FTMS:
                peripheral.discoverCharacteristics([
                    FitnessMachineFeatures.id,
                    FitnessMachineStatus.id,
                    IndoorBikeData.id,
                    FitnessMachineControlPoint.id,
                    SupportedSpeedRange.id,
                    SupportedInclinationRange.id,
                    SupportedResistanceLevelRange.id,
                    SupportedPowerRange.id,
                    SupportedHeartRateRange.id
                ], for: service)
            default:
                break
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if error != nil {
            print(error!)
            return
        }
        guard let chars = service.characteristics else {
            return
        }
        for char in chars {
            switch char.uuid {
            case FitnessMachineControlPoint.id:
                controlPoint = char
                peripheral.setNotifyValue(true, for: char)
            case IndoorBikeData.id:
                indoorBikeDataCharacteristic = char
                peripheral.setNotifyValue(true, for: char)
            case FitnessMachineStatus.id:
                fitnessMachineStatusCharacteristic = char
                peripheral.setNotifyValue(true, for: char)
            default:
                peripheral.readValue(for: char)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if characteristic.isNotifying {
            switch characteristic.uuid {
            case ControlPoint.id:
                guard let data = characteristic.value else { return }
                let response = ResponseCode(data)
                print("resOpCode: \(response.opCode) reqOpCode: \(response.requestOpCode) resultCode: \(response.resultCode)")

            case IndoorBikeData.id:
                guard let data = characteristic.value, data.count > 0 else { return }
                print("indoorBike:",[UInt8](data))
                let ib = indoorBikeData?.merge(data) ?? IndoorBikeData(data)
                DispatchQueue.main.async {
                    self.indoorBikeData = ib
                }

            case FitnessMachineStatus.id:
                guard let data = characteristic.value else { return }
                let char = FitnessMachineStatus(data)
                print("Fitnessmachine Status:\n", char.status, char.value ?? "")
                DispatchQueue.main.async {
                    self.fitnessMachineStatus = char
                }
            default:
                break
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print(error ?? "")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print(peripheralName(peripheral.identifier), characteristic.uuid, "is notifying = \(characteristic.isNotifying)", error ?? "")
    }
    
    func peripheral(_ id: UUID?) -> CBPeripheral? {
        guard let id = id else { return nil }
        if let peripheral = peripherals[id] {
            return peripheral
        }
        let peripherals = centralManager.retrievePeripherals(withIdentifiers: [id])
        if peripherals.count == 0 {
            return nil
        }
        return peripherals[0]
    }
    
    func peripheralName(_ id: UUID?) -> String {
        var name: String
        if let peripheral = peripheral(id), let str = peripheral.name {
            name = str
        } else {
            name = "no name"
        }
        if name.hasPrefix("JF") {
            let index = name.index(name.startIndex, offsetBy: 4)
            name = String(name[index...])
        }
        return name
    }
    
    func stateText(_ id: UUID?) -> String {
        guard let device = peripheral(id) else { return "unknow" }
        var stateText: String
        
        switch device.state {
        case .connecting:
            stateText = "Connecting"
        case .connected:
            stateText = "Connected"
        case .disconnecting:
            stateText = "NotConnected"
        case .disconnected:
            stateText = "NotConnected"
        default:
            stateText = "Unknow"
        }
        
        return stateText
    }
    
}
