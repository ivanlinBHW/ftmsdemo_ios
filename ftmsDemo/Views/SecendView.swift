//
//  SecendView.swift
//  basicfitDemo
//
//  Created by 廖健宇 on 2021/6/11.
//

import SwiftUI

struct SecendView: View {
    @ObservedObject var btManager = BluetoothManager.sharedInstance
    @Binding var peripheralId:UUID
    
    var body: some View {
        VStack {
            Text("Connect Status: \(btManager.stateText(peripheralId))")
                .padding()
            HStack(spacing:15) {
                Spacer()
                VStack {
                    Text("Distance")
                    Text("\(Double(btManager.indoorBikeData?.totalDistance ?? 0).toString(1)) km")
                }
                .padding()
                .background(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.gray, lineWidth: 1)
                )
                
                VStack {
                    Text("RPM")
                    Text("\(Double(btManager.indoorBikeData?.instantaneousCadence ?? 0).toString(0))")
                }
                .padding()
                .background(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.gray, lineWidth: 1)
                )
                VStack {
                    Text("Watt")
                    Text("\(btManager.indoorBikeData?.instantaneousPower ?? 0)")
                }
                .padding()
                .background(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.gray, lineWidth: 1)
                )
                Spacer()
            }
            .padding()
            Spacer()
        }
        .navigationTitle(btManager.peripheralName(peripheralId))
        .onAppear()
        {
            btManager.connect(peripheralId)
        }
        .onDisappear()
        {
            btManager.disConnectFitnessMachine()
        }
    }
}

struct SecendView_Previews: PreviewProvider {
    static var previews: some View {
        SecendView(peripheralId: .constant(UUID()))
    }
}
