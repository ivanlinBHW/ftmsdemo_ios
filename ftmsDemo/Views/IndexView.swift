//
//  IndexView.swift
//  basicfitDemo
//
//  Created by 廖健宇 on 2021/6/11.
//

import SwiftUI

struct IndexView: View {
    @ObservedObject var btManager = BluetoothManager.sharedInstance
    
    var body: some View {
        VStack {
            HStack {
                Button(action: {
                    btManager.startScan()
                }, label: {
                    HStack {
                        Spacer()
                        Text("Scan")
                            .font(.body)
                            .fontWeight(.bold)
                            .fixedSize(horizontal: true, vertical: true)
                            .foregroundColor(Color.white)
                            .padding(.vertical, 18)
                        Spacer()
                    }
                    .background(Color.blue)
                    .cornerRadius(20)
                    .padding(.horizontal, 30)
                })
                
                Button(action: {
                    btManager.stopScan()
                }, label: {
                    HStack {
                        Spacer()
                        Text("Stop Scan")
                            .font(.body)
                            .fontWeight(.bold)
                            .fixedSize(horizontal: true, vertical: true)
                            .foregroundColor(Color.white)
                            .padding(.vertical, 18)
                        Spacer()
                    }
                    .background(Color.blue)
                    .cornerRadius(20)
                    .padding(.horizontal, 30)
                })
            }
            .padding()
            
            List {
                ForEach(btManager.foundPeripheralList, id: \.self) { id in
                    NavigationLink(destination: SecendView(peripheralId: .constant(id))) {
                        HStack {
                            VStack (alignment: .leading){
                                Text(btManager.peripheralName(id))
                                    .font(.system(size: 18))
                                Text(btManager.peripheral(id)?.identifier.uuidString ?? "")
                                    .font(.system(size: 12))
                            }
                            Spacer()
                        }
                    }
                }
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("BLE Scan")
    }
}

struct IndexView_Previews: PreviewProvider {
    static var previews: some View {
        IndexView()
    }
}
