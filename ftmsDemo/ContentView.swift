//
//  ContentView.swift
//  ftmsDemo
//
//  Created by Steve Liao on 2021/7/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            IndexView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
